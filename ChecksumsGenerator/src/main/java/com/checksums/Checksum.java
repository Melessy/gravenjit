package com.checksums;

import com.google.gson.annotations.Expose;
import lombok.Builder;
import lombok.Value;

/**
 * Class representing a checksum
 */
@Value
@Builder
public class Checksum 
{
    @Expose
    String name;

    @Expose
    double version;

    @Expose
    long size;

    @Expose
    boolean execute;

    @Expose
    String creationTime;

    @Expose
    String lastModifiedTime;

    @Expose
    String lastAccessTime;

    @Expose
    String MD5;

    @Expose
    String SHA1;

    @Expose
    String SHA256;

    @Expose
    String SHA384;

    @Expose
    String SHA512;
}
