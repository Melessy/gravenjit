package com.checksums;

import com.checksums.utils.VersionUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.codec.digest.DigestUtils;

public class ChecksumsHandler 
{
    private final File[] excludedFiles = {Constants.CHECKSUMS_FILE}; // Exclude files added to Checksums
    
    /**
     * Get checksums from all files in directory and place them in a list
     *
     * @return checksumsList
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    private List getChecksumsList() throws NoSuchAlgorithmException, IOException 
    {
        File checksumsDir = Constants.CHECKSUMS_DIRECTORY;

        if (!checksumsDir.exists()) 
        {
            checksumsDir.mkdir();
            Constants.logger.log(Level.INFO, "Created directory: {0}", checksumsDir.getAbsolutePath());
        }

        List checksumsList = new ArrayList();

        // List of files
        ArrayList<File> listOfFiles = Files
                .list(checksumsDir.toPath()) // List files
                .map(Path::toFile) // Map Path.toFile(); defines ArrayList to contain files
                .filter(file -> !Arrays.asList(excludedFiles).contains(file)) // Filter excludedFiles
                .sorted() // Sort
                .collect(Collectors.toCollection(ArrayList::new)); // Collect to new ArrayList

        // List of files is empty
        if (listOfFiles.isEmpty()) 
        {
            Constants.logger.log(Level.WARNING, "{0} directory is empty!", Constants.CHECKSUMS_DIRECTORY);
            return null;
        }
        
        // Iterate through list of files and build checksums for each file
        listOfFiles.forEach((File file) -> 
        {
            try 
            {
                String fileName = file.getName();
                double fileVersion = VersionUtils.getVersionFromFile(file);
                long fileSize = file.length();
                boolean fileExecute = file.getName().endsWith(".jar");
                
                Instant fileCreationTime = Files.readAttributes(file.toPath(), BasicFileAttributes.class).creationTime().toInstant();
                Instant fileLastModifiedTime = Files.readAttributes(file.toPath(), BasicFileAttributes.class).lastModifiedTime().toInstant();
                Instant fileLastAccessTime = Files.readAttributes(file.toPath(), BasicFileAttributes.class).lastAccessTime().toInstant();
                DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault());
                
                String file_MD5 = DigestUtils.md5Hex(new FileInputStream(file));
                String file_SHA1 = DigestUtils.sha1Hex(new FileInputStream(file));
                String file_SHA256 = DigestUtils.sha256Hex(new FileInputStream(file));
                String file_SHA384 = DigestUtils.sha384Hex(new FileInputStream(file));
                String file_SHA512 = DigestUtils.sha512Hex(new FileInputStream(file));
                
                Checksum checksum = new Checksum.ChecksumBuilder()
                        .name(fileName)
                        .version(fileVersion)
                        .size(fileSize)
                        .execute(fileExecute)
                        .creationTime(DATE_TIME_FORMATTER.format(fileCreationTime))
                        .lastModifiedTime(DATE_TIME_FORMATTER.format(fileLastModifiedTime))
                        .lastAccessTime(DATE_TIME_FORMATTER.format(fileLastAccessTime))
                        .MD5(file_MD5)
                        .SHA1(file_SHA1)
                        .SHA256(file_SHA256)
                        .SHA384(file_SHA384)
                        .SHA512(file_SHA512)
                        .build();
                Constants.logger.log(Level.INFO, "Build checksum: {0}", checksum);
                checksumsList.add(checksum);
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(ChecksumsHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        return checksumsList;
    }

    /**
     * Generate JSON checksums file
     *
     * @throws IOException
     * @throws java.security.NoSuchAlgorithmException
     */
    void generateChecksums() throws IOException, NoSuchAlgorithmException 
    {
        File checksumsFile = Constants.CHECKSUMS_FILE;
        List checksumsList = getChecksumsList();
        
        if (checksumsFile == null || checksumsList == null) 
        {
            Constants.logger.log(Level.SEVERE, "Checksums file or checksums list is null!");
            Constants.logger.log(Level.SEVERE, "Failed to generate checksums!");
            return;
        }
        
        Constants.logger.log(Level.INFO, "Checksums: {0}", checksumsList.toString());
        try (Writer writer = new FileWriter(checksumsFile)) 
        {
            Gson gson = new GsonBuilder()
                    .excludeFieldsWithoutExposeAnnotation()
                    .setPrettyPrinting()
                    .create();
            gson.toJson(checksumsList, writer);
            Constants.logger.log(Level.INFO, "Successfully generated checksums file: {0}", checksumsFile.getAbsolutePath());
        }
    }
    
    /**
     * Get checksums from all files in directory and place them in a list
     *
     * @return checksumsList
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    /*private List getChecksumsList() throws NoSuchAlgorithmException, IOException 
    {
        File[] listOfFiles = Constants.CHECKSUMS_DIRECTORY.listFiles(); // List of all files in directory
        if (listOfFiles.length == 0) 
        {
            Constants.logger.log(Level.WARNING, "{0} is empty!", Constants.CHECKSUMS_DIRECTORY);
            return null;
        }

        List checksumsList = new ArrayList();
        for (File file : listOfFiles) 
        {
            if (Arrays.asList(excludedFiles).contains(file)) 
            {
                Constants.logger.log(Level.INFO, "Excluding file: {0}", file.getAbsolutePath());
                continue;
            }

            String fileName = file.getName();
            double fileVersion = VersionUtils.getVersionFromFile(file);
            long fileSize = file.length();
            boolean fileExecute = file.getName().endsWith(".jar");

            Instant fileCreationTime = Files.readAttributes(file.toPath(), BasicFileAttributes.class).creationTime().toInstant();
            Instant fileLastModifiedTime = Files.readAttributes(file.toPath(), BasicFileAttributes.class).lastModifiedTime().toInstant();
            Instant fileLastAccessTime = Files.readAttributes(file.toPath(), BasicFileAttributes.class).lastAccessTime().toInstant();
            DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault());

            String file_MD5 = DigestUtils.md5Hex(new FileInputStream(file));
            String file_SHA1 = DigestUtils.sha1Hex(new FileInputStream(file));
            String file_SHA256 = DigestUtils.sha256Hex(new FileInputStream(file));
            String file_SHA384 = DigestUtils.sha384Hex(new FileInputStream(file));
            String file_SHA512 = DigestUtils.sha512Hex(new FileInputStream(file));

            Checksum checksum = new Checksum.ChecksumBuilder()
                    .name(fileName)
                    .version(fileVersion)
                    .size(fileSize)
                    .execute(fileExecute)
                    .creationTime(DATE_TIME_FORMATTER.format(fileCreationTime))
                    .lastModifiedTime(DATE_TIME_FORMATTER.format(fileLastModifiedTime))
                    .lastAccessTime(DATE_TIME_FORMATTER.format(fileLastAccessTime))
                    .MD5(file_MD5)
                    .SHA1(file_SHA1)
                    .SHA256(file_SHA256)
                    .SHA384(file_SHA384)
                    .SHA512(file_SHA512)
                    .build();

            checksumsList.add(checksum);
        }
        return checksumsList;
    }*/
}
