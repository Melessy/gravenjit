package com.checksums.utils;

import com.checksums.Constants;
import java.io.File;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VersionUtils 
{
    /**
     * Get the double value of a file
     *
     * @param file
     * @return double value, version
     */
    public static double getVersionFromFile(File file) 
    {
        try 
        {
            Matcher matcher = Pattern.compile("(\\d\\.\\d)").matcher(file.getName()); // Get the double value of the file name
            matcher.find(); // Find the double
            double version = Double.parseDouble(matcher.group(1)); // Version found
            return version;
        } 
        catch (IllegalStateException ex) 
        {
            Constants.logger.log(Level.WARNING, "File: {0} doesn't contain a double!", file.getAbsolutePath());
            //Logger.getLogger(VersionUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0.0;
    }
}
