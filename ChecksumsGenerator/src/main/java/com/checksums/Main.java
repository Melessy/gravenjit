package com.checksums;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class Main 
{
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException 
    {
        ChecksumsHandler checksumsHandler = new ChecksumsHandler();
        checksumsHandler.generateChecksums();
    }
}
