package com.checksums;

import java.io.File;
import java.util.logging.Logger;

public class Constants 
{
    // Configuration
    public static final File CHECKSUMS_DIRECTORY = new File("./checksums");
    public static final File CHECKSUMS_FILE = new File(CHECKSUMS_DIRECTORY, "Checksums.json");

    // Logger
    public static final Logger logger;

    static 
    {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%3$s.%4$-4s] [%2$s] -> %5$s %6$s %n"); // Set formatted properties
        logger = Logger.getLogger("Logger"); // Initialize Logger
    }
}
