/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.webserver;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelProgressiveFuture;
import io.netty.channel.ChannelProgressiveFutureListener;
import io.netty.channel.DefaultFileRegion;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpChunkedInput;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.stream.ChunkedFile;
import io.netty.util.CharsetUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.time.Instant;
import java.util.regex.Pattern;
import javax.activation.MimetypesFileTypeMap;

/**
 * @author : jy.chen
 * @version : 1.0
 * @since : 2016-12-05 11:02
 * @see <https://github.com/wenzhucjy/netty-tutorial/tree/master/netty-4/server/src/main/java/com/netty/httpfile>
 *
 * @author : Melessy
 * @version 1.1
 * @since : 2020-11-23
 * Simple WebServer
 * @TODO refactor, logging, merge to java 8, proper configuration
 * @DONE View plainTextFiles instead of transfering them direcetly,
 * modified caching, added error html pages, refactored a bit.
 *
 */
public class HttpServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> 
{
    private final String url; // The url/link of our content
    private final Pattern ALLOWED_FILE_NAME = Pattern.compile("[A-Za-z0-9][-_A-Za-z0-9\\.]*"); // Allowed file names
    private final Pattern INSECURE_URI = Pattern.compile(".*[<>&\"].*"); // Insecure URI's
    private final int HTTP_CACHE_SECONDS = 60; // Max cache seconds before expire

    private final String[] plainTextFiles = {".json", ".txt", ".xml"}; // View plainText files content in browser instead of transfering them directly
    private final int MAX_TEXT_SIZE = 5 * 1024 * 1024; // Max textSize to display in browser, 5MB

    public HttpServerHandler(String url) 
    { // constructor
        this.url = url;
    }

    /**
     * Read from channel
     *
     * @param chc
     * @param request
     * @throws Exception
     */
    @Override
    protected void channelRead0(ChannelHandlerContext chc, FullHttpRequest request) throws Exception 
    {
        if (!request.decoderResult().isSuccess()) 
        { // 400
            sendError(chc, HttpResponseStatus.BAD_REQUEST);
            return;
        }

        if (request.method() != HttpMethod.GET) 
        { // 405
            sendError(chc, HttpResponseStatus.METHOD_NOT_ALLOWED);
            return;
        }

        String uri = request.uri();
        String path = sanitizeUri(uri);
        if (path == null) 
        { // 403
            sendError(chc, HttpResponseStatus.FORBIDDEN);
            return;
        }

        File file = new File(path);
        if (file.isHidden() || !file.exists()) 
        { // 404
            sendError(chc, HttpResponseStatus.NOT_FOUND);
            return;
        }

        if (file.isDirectory()) 
        { // Do directory listing
            if (uri.endsWith("/")) 
            {
                sendListing(chc, file);
            } 
            else 
            {
                sendRedirect(chc, uri + '/');
            }
            return;
        }

        if (!file.isFile()) 
        {
            // 403
            sendError(chc, HttpResponseStatus.FORBIDDEN);
            return;
        }

        // Cache Validation using epoch seconds
        String modifiedSince = request.headers().get(HttpHeaderNames.IF_MODIFIED_SINCE);
        if (modifiedSince != null && !modifiedSince.isEmpty()) 
        {
            Instant modifiedSinceEpochSecond = Instant.parse(modifiedSince);
            Instant fileLastModifiedEpochSecond = Instant.ofEpochSecond(file.lastModified());
            if (modifiedSinceEpochSecond.equals(fileLastModifiedEpochSecond)) 
            {
                sendNotModified(chc);
                return;
            }
        }

        /**
         * View plain-text file content in browser
         */
        for (String fileEnd : plainTextFiles) 
        {
            if (file.getName().endsWith(fileEnd) && file.length() <= MAX_TEXT_SIZE) 
            {
                String fileContent = new String(Files.readAllBytes(file.toPath()));
                FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, Unpooled.copiedBuffer(fileContent.getBytes()));
                //response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaders.Values.KEEP_ALIVE); // Deprecated
                response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
                response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain; charset=UTF-8");
                response.headers().set(HttpHeaderNames.CONTENT_LENGTH, fileContent.length());
                setDateAndCacheHeaders(response, file);
                chc.writeAndFlush(response);
                return;
            }
        }

        /**
         * Transfer file to client/browser
         */
        RandomAccessFile raf;
        try 
        {
            raf = new RandomAccessFile(file, "r");
        } 
        catch (FileNotFoundException ignore) 
        {
            // 404
            sendError(chc, HttpResponseStatus.NOT_FOUND);
            return;
        }
        long fileLength = raf.length();
        HttpResponse response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
        HttpUtil.setContentLength(response, fileLength);
        setContentTypeHeader(response, file);
        setDateAndCacheHeaders(response, file);
        if (HttpUtil.isKeepAlive(request)) 
        {
            response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
        }

        chc.write(response);

        ChannelFuture sendFileFuture;
        if (chc.pipeline().get(SslHandler.class) == null) 
        {
            sendFileFuture = chc.write(new DefaultFileRegion(raf.getChannel(), 0, fileLength),
                    chc.newProgressivePromise());
        } 
        else 
        {
            sendFileFuture = chc.write(new HttpChunkedInput(new ChunkedFile(raf, 0, fileLength, 8192)),
                    chc.newProgressivePromise());
        }

        sendFileFuture.addListener(new ChannelProgressiveFutureListener() 
        {
            @Override
            public void operationProgressed(ChannelProgressiveFuture future, long progress, long total) 
            {
                if (total < 0) 
                { // total unknown
                    System.err.println(future.channel() + " Transfer progress: " + progress);
                } 
                else 
                {
                    System.err.println(future.channel() + " Transfer progress: " + progress + " / " + total);
                }
            }

            @Override
            public void operationComplete(ChannelProgressiveFuture future) throws IOException 
            {
                System.err.println(future.channel() + " Transfer complete.");
                raf.close(); // close raf when transfer completed
            }
        });

        ChannelFuture lastContentFuture = chc.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);

        if (!HttpUtil.isKeepAlive(request)) 
        {
            // Close the connection when the whole content is written out.
            lastContentFuture.addListener(ChannelFutureListener.CLOSE);
        }
    }

    /**
     * Set date and cache headers
     *
     * @param response
     * @param fileToCache
     */
    private void setDateAndCacheHeaders(HttpResponse response, File fileToCache) 
    {
        Instant currentDateTime = Instant.now(); // Current date&time instant
        response.headers().set(HttpHeaderNames.DATE, currentDateTime); // Date header
        Instant expire = currentDateTime.plusSeconds(HTTP_CACHE_SECONDS); // Expire
        response.headers().set(HttpHeaderNames.EXPIRES, expire); // Set expire
        response.headers().set(HttpHeaderNames.CACHE_CONTROL, "private, max-age=" + HTTP_CACHE_SECONDS); // Cache control
        response.headers().set(HttpHeaderNames.LAST_MODIFIED, Instant.ofEpochSecond(fileToCache.lastModified())); // File last modified
        System.out.println(response.headers()); // Print headers
    }

    /**
     * Send not modified
     *
     * @param chc
     */
    private void sendNotModified(ChannelHandlerContext chc) 
    {
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_MODIFIED);
        setDateHeader(response);
        chc.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

    /**
     * Send redirection to new URI
     *
     * @param chc
     * @param newUri
     */
    private static void sendRedirect(ChannelHandlerContext chc, String newUri) 
    {
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.FOUND);
        response.headers().set(HttpHeaderNames.LOCATION, newUri);
        chc.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

    /**
     * Send directory listing using HTML for style and java for the listing
     *
     * @param chc
     * @param dir
     * @throws IOException
     */
    private void sendListing(ChannelHandlerContext chc, File dir) throws IOException 
    {
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/html; charset=UTF-8");

        StringBuilder buf = new StringBuilder(); // Initialize StringBuilder buf
        File indexFile = new File("./" + File.separator + url + File.separator + "index.html"); // Index file
        byte[] fileContent = Files.readAllBytes(indexFile.toPath()); // Read all content from index file
        buf.append(new String(fileContent)); // Append the html index file content converted from bytes to string using new String();

        buf.append("<!DOCTYPE html>\r\n");
        File[] files = dir.listFiles();
        if (files != null && files.length > 0) 
        {
            for (File f : files) 
            {
                if (f.isHidden() || !f.canRead()) 
                {
                    continue;
                }

                String name = f.getName();
                if (!ALLOWED_FILE_NAME.matcher(name).matches()) 
                {
                    continue;
                }

                buf.append("<li><a href=\"");
                buf.append(name);
                buf.append("\">");
                buf.append(name);
                buf.append("</a></li>\r\n");
            }
        }
        buf.append("</ul></body></html>\r\n");
        ByteBuf buffer = Unpooled.copiedBuffer(buf, CharsetUtil.UTF_8);
        response.content().writeBytes(buffer);
        buffer.release();
        chc.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

    /**
     * Sanitize URI
     *
     * @param uri
     * @return path to directory
     */
    private String sanitizeUri(String uri) 
    {
        try 
        {
            uri = URLDecoder.decode(uri, CharsetUtil.UTF_8.displayName());
        } 
        catch (UnsupportedEncodingException e) 
        {
            try 
            {
                uri = URLDecoder.decode(uri, CharsetUtil.ISO_8859_1.displayName());
            } 
            catch (UnsupportedEncodingException ex) 
            {
                throw new Error();
            }
        }

        if (!uri.startsWith(url) || !uri.startsWith("/")) 
        {
            return null;
        }

        uri = uri.replace('/', File.separatorChar);
        if (uri.contains(File.separator + ".") || uri.contains("." + File.separator) || uri.startsWith(".")
                || uri.endsWith(".") || INSECURE_URI.matcher(uri).matches()) 
        {
            return null;
        }

        return System.getProperty("user.dir") + File.separator + uri;
    }

    /**
     * Set Content type header
     *
     * @param response
     * @param file
     */
    private static void setContentTypeHeader(HttpResponse response, File file) 
    {
        MimetypesFileTypeMap map = new MimetypesFileTypeMap();
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, map.getContentType(file.getPath()));
    }

    /**
     * Set current date time header
     *
     * @param response
     */
    private void setDateHeader(FullHttpResponse response) 
    {
        Instant currentDateTime = Instant.now();
        response.headers().set(HttpHeaderNames.DATE, currentDateTime);
    }

    /**
     * Send error using error HTML files
     *
     * @param chc
     * @param status
     */
    private void sendError(ChannelHandlerContext chc, HttpResponseStatus status) throws IOException 
    {
        File errorPage = new File("./" + File.separator + "error-pages" + File.separator + status.code() + ".html"); // HTML error page
        byte[] fileContent = Files.readAllBytes(errorPage.toPath());
        ByteBuf byteBuf = Unpooled.copiedBuffer(fileContent);
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status, byteBuf);
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/html;charset=UTF-8");
        chc.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext chc, Throwable cause) throws IOException 
    {
        cause.printStackTrace();
        if (chc.channel().isActive()) 
        {
            sendError(chc, HttpResponseStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
