# GravenJIT
Graven Just-In-Time
Java Remote Control Program

## GravenJIT module
* Client, communicate with the server.
* Server, handles the client.
* Updater, updates from checksums on the Webserver.
* Webserver, hosts: Maven2 repositories and Updater checksums and files to download in '/content' folder.
* ChecksumsGenerator, Generate JSON checksums for all files in a directory.

### HOW-TO

```
1. Open as maven project
2. Read every class to get a understanding
3. Change some constants and configurations
4. Modify to your needs
5. ...Fill this in
```



