/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.server.tasks.impl;

import com.gravenjit.client.Client;
import com.gravenjit.server.ChannelHandler;
import com.gravenjit.server.Constants;
import com.gravenjit.server.tasks.Task;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @deprecated Currently only does ScreenCapture, Mouse and Key listeners not
 * properly implemented. I don't see the use for why to implement it yet.
 */
public class RemoteDesktopTask implements Task, MouseMotionListener, MouseListener 
{

    private JFrame rDesktopFrame = new JFrame();
    private JDesktopPane desktopPane = new JDesktopPane();
    public JPanel rDesktopPanel = new JPanel();
    private String identity;
    private boolean cancel = false;

    public void drawFrame() 
    {
        rDesktopFrame.setTitle("Remote Desktop");
        rDesktopFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        rDesktopFrame.setPreferredSize(new Dimension(600, 400));
        rDesktopFrame.setBounds(700, 200, 0, 0);
        rDesktopFrame.add(desktopPane, BorderLayout.CENTER);
        rDesktopFrame.getContentPane().add(rDesktopPanel, BorderLayout.CENTER);
        rDesktopFrame.setFocusable(true);
        rDesktopFrame.pack();
        rDesktopFrame.setVisible(true);
        rDesktopFrame.addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                cancel = true;
                rDesktopFrame.dispose();
                ChannelHandler.writeOne(identity, "RDP cancel");
                try 
                {
                    Thread.sleep(2000);
                } 
                catch (InterruptedException ex) 
                {
                    Logger.getLogger(RemoteDesktopTask.class.getName()).log(Level.SEVERE, null, ex);
                } 
                finally 
                {
                    cancel = false;
                    Constants.getLogger().log(Level.INFO, "canceled RDP: {0}", identity);
                    //Thread.currentThread().interrupt();
                }
            }
        });
        rDesktopPanel.addMouseListener(this); // Add MouseListener to panel
        rDesktopPanel.addMouseMotionListener(this);
    }

    @Override
    public void execute(Client client) 
    {
        if (!rDesktopFrame.isVisible() && !cancel) 
        {
            drawFrame();
        }

        try 
        {
            this.identity = client.getUsername();
            Image image = ImageIO.read(new ByteArrayInputStream(client.getFileContent()));
            if (image != null) 
            {
                image = image.getScaledInstance(rDesktopPanel.getWidth(), rDesktopPanel.getHeight(), Image.SCALE_FAST);
                Graphics graphics = rDesktopPanel.getGraphics();
                graphics.drawImage(image, 0, 0, rDesktopPanel.getWidth(), rDesktopPanel.getHeight(), rDesktopPanel);
            }
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(RemoteDesktopTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void mouseDragged(MouseEvent e) 
    {

    }

    @Override
    public void mouseMoved(MouseEvent e) 
    {
        //ChannelHandler.writeOne(identity, "RDP mouseMoved " + e.getLocationOnScreen());
    }

    public void mouseClicked(MouseEvent e) 
    {

    }

    @Override
    public void mousePressed(MouseEvent e) 
    {
        //ChannelHandler.writeOne(identity, "RDP mousePressed " + e.getButton());
    }

    public void mouseReleased(MouseEvent e) 
    {

    }

    public void mouseEntered(MouseEvent e) 
    {

    }

    public void mouseExited(MouseEvent e) 
    {

    }
}
