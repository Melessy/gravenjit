/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.server.tasks;

import com.gravenjit.client.Client;
import com.gravenjit.server.Constants;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Handle Tasks received from client
 */
public class TaskHandler implements Runnable 
{
    private final ExecutorService taskExecutor = Executors.newSingleThreadExecutor(); // Executor to submit Future<Task>

    private final Client client;

    /**
     * Execute a task received from client
     *
     * @param client
     */
    public TaskHandler(Client client) 
    {
        this.client = client;
    }

    @Override
    public void run() 
    {
        String[] args = client.getMessage().split(" ");
        String task = args[0];

        if (!Stream.of(Tasks.values()).map(Enum::name).collect(Collectors.toSet()).contains(task)) { // Invalid task
            //System.out.println("Invalid task: " + task);
            //Constants.getLogger().log(Level.WARNING, "Invalid task: {0}", task);
            return;
        }

        Instant startTime = Instant.now(); // Start time
        Task t = Tasks.valueOf(task).getTask(); // Get task
        Constants.getLogger().log(Level.INFO, "Executing task {0}", t);

        Future<Task> future = (Future<Task>) taskExecutor.submit(() -> 
        { // Use Future to execute task and get its result
            t.execute(client); // Execute the task
            return t; // Return the task
        });

        try 
        {
            future.get(5, TimeUnit.MINUTES); // Wait max x amount of time for task to finish
        } 
        catch (InterruptedException | ExecutionException | TimeoutException ex) 
        {
            Logger.getLogger(TaskHandler.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally 
        {
            if (future.isDone()) 
            { // If task is completed/canceled
                Instant endTime = Instant.now(); // This is the endTime
                long totalTime = Duration.between(startTime, endTime).toMillis(); // This is the totalTime it took
                Constants.getLogger().log(Level.INFO, "Task completed: {0} in {1} ms", new Object[]{t, totalTime});
                System.out.println(taskExecutor.toString()); // Print executor status
                taskExecutor.shutdownNow(); // Shutdown taskExecutor
            }
        }
    }
}
