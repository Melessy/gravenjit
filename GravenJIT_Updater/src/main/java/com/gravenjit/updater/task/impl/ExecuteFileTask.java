/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.updater.task.impl;

import com.gravenjit.updater.Constants;
import com.gravenjit.updater.task.Task;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Task to execute a file
 */
public class ExecuteFileTask implements Task 
{
    private final File file;

    public ExecuteFileTask(File file) 
    {
        this.file = file;
    }

    @Override
    public void execute() 
    {
        if (file == null) 
        {
            Constants.logger.log(Level.SEVERE, "File to execute is null!");
            return;
        }
        
        if (file.getName().endsWith(".jar")) 
        {
            try 
            {
                Constants.logger.log(Level.INFO, "Executing file: {0}", file.getAbsolutePath());
                new ProcessBuilder
        (
                        "java",
                        "-jar",
                        "-Xmx256m",
                        file.getAbsolutePath())
                        .start();
            } 
            catch (IOException failedToExecute) 
            {
                Logger.getLogger(ExecuteFileTask.class.getName()).log(Level.SEVERE, null, failedToExecute);
                try 
                {
                    Desktop.getDesktop().open(file);
                } 
                catch (IOException failedToOpenAfterExecuteFailed) 
                {
                    Logger.getLogger(ExecuteFileTask.class.getName()).log(Level.SEVERE, null, failedToOpenAfterExecuteFailed);
                }
            }
        } 
        else 
        {
            try 
            {
                if (!Desktop.isDesktopSupported()) 
                {
                    Constants.logger.log(Level.WARNING, "Desktop not supported!, cannot open file: {0}", file);
                    return;
                }
                Constants.logger.log(Level.INFO, "Opening file: {0}", file.getAbsolutePath());
                Desktop.getDesktop().open(file);
            } 
            catch (IOException failedToOpen) 
            {
                Logger.getLogger(ExecuteFileTask.class.getName()).log(Level.SEVERE, null, failedToOpen);
            }
        }
    }
}
