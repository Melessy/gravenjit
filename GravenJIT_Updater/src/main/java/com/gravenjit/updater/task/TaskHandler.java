/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.updater.task;

import com.gravenjit.updater.Constants;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles tasks
 */
public class TaskHandler 
{
    private ExecutorService taskExecutor;
    private Future<Task> future;
    
    public void runTask(Task task) 
    {
        Instant startTime = Instant.now(); // Start time
        taskExecutor = Executors.newSingleThreadExecutor(); // Task executor
        
        future = (Future<Task>) taskExecutor.submit(() -> 
        { // Use Future to execute tasks and get its result
            Constants.logger.log(Level.INFO, "Running task: [{0}] {1}", new Object[]{Thread.currentThread().getName(), task});
            task.execute(); // Run task
            return task; // Return task
        });

        try 
        {
            future.get(10, TimeUnit.MINUTES); // Wait x amount of time for task to finish, 10 minutes for downloaderTask?
        }
        catch (InterruptedException | ExecutionException | TimeoutException ex) 
        {
            Logger.getLogger(TaskHandler.class.getName()).log(Level.SEVERE, null, ex);
            taskExecutor.shutdownNow();
        } 
        finally 
        {
            if (future.isDone()) 
            { // If task is completed
                Instant endTime = Instant.now(); // This is the endTime
                long totalTime = Duration.between(startTime, endTime).toMillis(); // This is the totalTime it took
                Constants.logger.log(Level.INFO, "Task completed: {0} in {1} ms", new Object[]{task, totalTime});
                taskExecutor.shutdownNow(); // Shutdown executor
            } 
            else 
            {
                Constants.logger.log(Level.WARNING, "Task canceled: {0}", new Object[]{task});
            }
        }
    }
}
