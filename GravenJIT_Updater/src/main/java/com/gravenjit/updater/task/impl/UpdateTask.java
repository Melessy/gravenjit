/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.updater.task.impl;

import com.gravenjit.updater.Constants;
import com.gravenjit.updater.checksums.Checksum;
import com.gravenjit.updater.checksums.ChecksumsHandler;
import com.gravenjit.updater.links.Links;
import com.gravenjit.updater.task.Task;
import com.gravenjit.updater.task.TaskHandler;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;

public class UpdateTask implements Task 
{
    Links links;
    
    public UpdateTask(Links links) 
    {
        this.links = links;
    }

    /**
     * check and do update
     *
     * @param links
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws java.net.MalformedURLException
     * @throws java.net.URISyntaxException
     */
    private void update() throws NoSuchAlgorithmException, IOException, MalformedURLException, URISyntaxException 
    {
        Constants.logger.log(Level.INFO, "Checking for updates: {0}", links.getChecksumsLink());
        ChecksumsHandler checksumsHandler = new ChecksumsHandler(); // Initialize new checksumsHandler
        List<Checksum> checksumsList = checksumsHandler.getChecksumsList(links.getChecksumsLink()); // Get checksumsList
        TaskHandler taskHandler = new TaskHandler(); // Initialize new taskHandler

        if (checksumsList == null) 
        {
            Constants.logger.log(Level.SEVERE, "Failed to update: Checksum list is null!");
            return;
        }

        // Stream
        /*List<Checksum> checksumsListStream = checksumsList.stream()
                //.map(Checksum::getName)
                .filter(checksum -> checksum != null)
                .sorted(Comparator.comparing(Checksum::getName))
                .collect(Collectors.toList());*/
        
        // Sort the checksumsList using Comparator, ClassName::getFieldName
        checksumsList.sort(Comparator.comparing(Checksum::getName));
        // Iterate over all checksums in the checksumsList
        checksumsList.forEach((Checksum checksum) -> 
        {
            try 
            {
                Constants.logger.log(Level.INFO, checksum.toString());
                String downloadLink = (String) links.getDownloadLinks().get(checksum.getName());

                if (downloadLink == null) 
                {
                    Constants.logger.log(Level.WARNING, "Download link from file: {0} is null", checksum.getName());
                    Constants.logger.log(Level.SEVERE, "Failed to update file: {0}", checksum.getName());
                    return;
                }

                File myFile = new File(Constants.FILE_STORE + File.separator + checksum.getName());

                // File doesn't exist
                if (!myFile.exists()) 
                {
                    Constants.logger.log(Level.INFO, "Update available for: {0}", checksum.getName());
                    taskHandler.runTask(new DownloadTask(downloadLink)); // Download from downloadLink
                }

                // Verify all hashes
                if (!isVerifiedHash(myFile, checksum)) 
                {
                    Constants.logger.log(Level.WARNING, "Invalid hash found for: {0}", myFile.getAbsolutePath());
                    myFile.delete(); // Delete existing file
                    taskHandler.runTask(new DownloadTask(downloadLink)); // Download from downloadLink
                } 
                else 
                {
                    Constants.logger.log(Level.INFO, "Successfully verified all hashes from file: {0}", myFile.getAbsolutePath());
                    Constants.logger.log(Level.INFO, "No update available for: {0}", myFile.getAbsolutePath());
                }

                // Execute file
                if (!checksum.isExecute()) 
                {
                    return;
                }

                if (checksum.isExecute() && !Constants.FILES_ALREADY_EXECUTED.contains(myFile.getName())) 
                {
                    taskHandler.runTask(new ExecuteFileTask(myFile)); // Run ExecuteFileTask
                    Constants.FILES_ALREADY_EXECUTED.add(myFile.getName());
                } 
                else 
                {
                    Constants.logger.log(Level.INFO, "File: {0} was already executed, not executing again", myFile.getName());
                }
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(UpdateTask.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Verify hashes
     *
     * @param file
     * @param checksum
     * @return isVerifiedHash boolean
     * @throws FileNotFoundException
     * @throws IOException
     */
    public boolean isVerifiedHash(File file, Checksum checksum) throws FileNotFoundException, IOException 
    {
        String myFile_MD5 = DigestUtils.md5Hex(new FileInputStream(file));
        String myFile_SHA1 = DigestUtils.sha1Hex(new FileInputStream(file));
        String myFile_SHA256 = DigestUtils.sha256Hex(new FileInputStream(file));
        String myFile_SHA384 = DigestUtils.sha384Hex(new FileInputStream(file));
        String myFile_SHA512 = DigestUtils.sha512Hex(new FileInputStream(file));

        if (file == null || checksum == null) 
        {
            return false;
        }

        // MD5
        if (myFile_MD5 == null ? checksum.getMD5() == null : !myFile_MD5.equals(checksum.getMD5())) 
        {
            Constants.logger.log(Level.WARNING, "Invalid MD5 hash: {0} -> {1} != {2}", new Object[]{file.getAbsolutePath(), checksum.getMD5(), myFile_MD5});
            return false;
        }
        Constants.logger.log(Level.INFO, "Verified MD5 hash: {0} -> {1}", new Object[]{file.getAbsolutePath(), checksum.getMD5()});

        // SHA1
        if (myFile_SHA1 == null ? checksum.getSHA1() == null : !myFile_SHA1.equals(checksum.getSHA1())) 
        {
            Constants.logger.log(Level.WARNING, "Invalid SHA1 hash: {0} -> {1} != {2}", new Object[]{file.getAbsolutePath(), checksum.getSHA1(), myFile_SHA1});
            return false;
        }
        Constants.logger.log(Level.INFO, "Verified SHA1 hash: {0} -> {1}", new Object[]{file.getAbsolutePath(), checksum.getSHA1()});

        // SHA256
        if (myFile_SHA256 == null ? checksum.getSHA256() == null : !myFile_SHA256.equals(checksum.getSHA256())) 
        {
            Constants.logger.log(Level.WARNING, "Invalid SHA256 hash: {0} -> {1} != {2}", new Object[]{file.getAbsolutePath(), checksum.getSHA256(), myFile_SHA256});
            return false;
        }
        Constants.logger.log(Level.INFO, "Verified SHA256 hash: {0} -> {1}", new Object[]{file.getAbsolutePath(), checksum.getSHA256()});

        // SHA384
        if (myFile_SHA384 == null ? checksum.getSHA384() == null : !myFile_SHA384.equals(checksum.getSHA384())) 
        {
            Constants.logger.log(Level.WARNING, "Invalid SHA384 hash: {0} -> {1} != {2}", new Object[]{file.getAbsolutePath(), checksum.getSHA384(), myFile_SHA384});
            return false;
        }
        Constants.logger.log(Level.INFO, "Verified SHA384 hash: {0} -> {1}", new Object[]{file.getAbsolutePath(), checksum.getSHA384()});

        // SHA512
        if (myFile_SHA512 == null ? checksum.getSHA512() == null : !myFile_SHA512.equals(checksum.getSHA512())) 
        {
            Constants.logger.log(Level.WARNING, "Invalid SHA512 hash: {0} -> {1} != {2}", new Object[]{file.getAbsolutePath(), checksum.getSHA512(), myFile_SHA512});
            return false;
        }
        Constants.logger.log(Level.INFO, "Verified SHA512 hash: {0} -> {1}", new Object[]{file.getAbsolutePath(), checksum.getSHA512()});
        return true;
    }

    @Override
    public void execute() 
    {
        try 
        {
            update();
        } 
        catch (NoSuchAlgorithmException | IOException | URISyntaxException ex) 
        {
            Logger.getLogger(UpdateTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
