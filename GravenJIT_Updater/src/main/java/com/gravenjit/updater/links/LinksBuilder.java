/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.updater.links;

import com.gravenjit.updater.Constants;
import com.gravenjit.updater.checksums.Checksum;
import com.gravenjit.updater.checksums.ChecksumsHandler;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class LinksBuilder implements Callable<Links> 
{
    private Links links;
    
    /**
     * Get available checksums link
     *
     * @return checksumsLink
     */
    private String getChecksumsLink() 
    {
        // Iterate through rootLinksArray
        for (String rootLink : Constants.ROOT_LINKS_ARRAY) 
        {
            String checksumsLink = String.format(rootLink + Constants.CHECKSUM_NAME); // checksum link
            
            // Check if checksum link is available
            if (!isHTTP_OK(checksumsLink)) 
            {
                Constants.logger.log(Level.WARNING, "Checksum link unavailable {0}", checksumsLink);
                continue;
            }
            Constants.logger.log(Level.INFO, "Checksum link available {0}", checksumsLink);
            return checksumsLink;
        }
        return null;
    }

    /**
     * Get available download links
     *
     * @return downloadLinks map
     * @throws URISyntaxException
     */
    private Map getDownloadLinks(String checksumsLink) throws URISyntaxException 
    {
        ChecksumsHandler checksumsHandler = new ChecksumsHandler(); // Initialize checksumHandler
        List<Checksum> checksumsList = checksumsHandler.getChecksumsList(checksumsLink); // Get checksums list
        Map<String, String> downloadLinksMap = new LinkedHashMap(); // Initialize new downloadLinks LinkedHashMap to keep it ordered
        
        if (checksumsList == null) 
        {
            Constants.logger.log(Level.SEVERE, "Checksums list is null!");
            return null;
        }
        
        // Stream
        List<Checksum> checksumsListStream = checksumsList.stream()
                //.map(Checksum::getName)
                .filter(checksum -> checksum != null)
                .sorted(Comparator.comparing(Checksum::getName))
                .collect(Collectors.toList());
        
        // Iterate over all checksums in the checksumList
        checksumsListStream.forEach((Checksum checksum) -> 
        {
            try 
            {
                String rootLink = checksumsLink.replace(Constants.CHECKSUM_NAME, ""); // Ugly but working method to define rootLink
                String downloadLink = String.format(rootLink + checksum.getName()); // Set downloadLink using fileName from checksum
                
                // Check for available download link
                if (!isHTTP_OK(downloadLink))
                {
                    Constants.logger.log(Level.WARNING, "Download link unavailable {0}", downloadLink);
                    return;
                }
                Constants.logger.log(Level.INFO, "Download link available {0}", downloadLink);
                String fileName = Paths.get(new URI(downloadLink).getPath()).getFileName().toString(); // Get fileName from url
                downloadLinksMap.put(fileName, downloadLink); // Add available download link to downloadLinks map
            } 
            catch (URISyntaxException ex) 
            {
                Logger.getLogger(LinksBuilder.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        return downloadLinksMap;
    }

    /**
     * Build available checksums & download links
     *
     * @throws java.net.URISyntaxException
     * @see Links.class
     */
    private void build() throws URISyntaxException 
    {
        String checksumsLink = getChecksumsLink();
        if (checksumsLink == null) 
        {
            Constants.logger.log(Level.SEVERE, "No checksums link available!");
            return;
        }
        
        Map<String, String> downloadLinksMap = getDownloadLinks(checksumsLink);
        if (downloadLinksMap == null) 
        {
            Constants.logger.log(Level.WARNING, "No download links available!");
        }
        
        // Stream, sorted
        /*Map<String, String> downloadLinksMapStream = downloadLinksMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));*/

        Constants.logger.log(Level.INFO, "Building links...");
        this.links = new Links.LinksBuilder()
                .checksumsLink(checksumsLink)
                .downloadLinks(downloadLinksMap)
                .build();

        if (links != null) 
        {
            Constants.logger.log(Level.INFO, "Successfully build links: {0}", links.toString());
        } 
        else 
        {
            Constants.logger.log(Level.WARNING, "Building links failed!");
        }
    }

    /**
     * Check if link is available
     *
     * @param link
     * @return HttpURLConnection.HTTP_OK
     */
    private boolean isHTTP_OK(String link) 
    {
        try 
        {
            URL url = new URL(link); // Set the url using String link
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.0.13) Gecko/2009073021 Firefox/3.0.13");
            int responseCode = connection.getResponseCode();
            connection.disconnect();
            return responseCode == HttpURLConnection.HTTP_OK;
        } 
        catch (MalformedURLException ignore) 
        {
        } 
        catch (IOException ignore) 
        {
        }
        return false;
    }

    @Override
    public Links call() throws Exception 
    {
        build();
        
        if (links != null) 
        {
            return links;
        }
        return null;
    }
}
