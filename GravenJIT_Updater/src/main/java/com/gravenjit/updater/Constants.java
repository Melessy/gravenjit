/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.updater;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

public class Constants 
{
    // System properties
    public static String homeDir = System.getProperty("user.home");
    public static String seperator = System.getProperty("file.separator");
    public static String envRootDir = System.getProperty("user.dir");
    public static String tempDir = System.getProperty("java.io.tmpdir");
    public static String desktopDir = homeDir + seperator + "Desktop";
    public static String username = System.getProperty("user.name");
    public static String javaHome = System.getProperty("java.home");
    
    // Configuration
    public static final File FILE_STORE = new File(tempDir, "GravenJIT_Cache"); // Here we store downloaded files
    public static final int UPDATE_SCHEDULE_INTERVAL = 20; // Amount of minutes to wait before performing updates again
    
    /**
     * Root links array / root directory
     * 
     * Used to get Checksums from rootLink + CHECKSUM_NAME and download files from rootLink + Checksum.getName()
     * @see LinksBuilder.class
     */
    public final static String[] ROOT_LINKS_ARRAY = 
    {
        "http://merdfd.duckdns.org:8080/content/updaterCache/", 
        "http://melessy.duckdns.org:8080/content/updaterCache/", 
        "http://melds.duckdns.org:8080/content/updaterCache/", 
        "http://melkova.duckdns.org:8080/content/updaterCache/", 
        "http://127.0.0.1:8080/content/updaterCache/"
    };
    
    // Checksum name, this file must be in the root directory
    public final static String CHECKSUM_NAME = "Checksums.json";
    
    // Logger
    public static final Logger logger;
    
    static 
    {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%3$s.%4$-4s] [%2$s] -> %5$s %6$s %n"); // Set formatted properties
        logger = Logger.getLogger("Logger"); // Initialize Logger
    }
    
    // Misc
    public static final Set<String> FILES_ALREADY_EXECUTED = new HashSet(); // Files already executed, so when we schedule we don't execute same files
    
}
