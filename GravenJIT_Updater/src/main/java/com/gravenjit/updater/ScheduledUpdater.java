/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.updater;

import com.gravenjit.updater.links.Links;
import com.gravenjit.updater.links.LinksBuilder;
import com.gravenjit.updater.task.TaskHandler;
import com.gravenjit.updater.task.impl.UpdateTask;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ScheduledUpdater extends TimerTask
{
    LinksBuilder linksBuilder = new LinksBuilder(); // LinksBuilder instance
    TaskHandler taskHandler = new TaskHandler(); // Initialize taskHandler
    
    @Override
    public void run() 
    {
        Constants.logger.log(Level.INFO, "Checking for available links...");
        try 
        {
            Links links = linksBuilder.call();
            if (links == null) 
            {
                Constants.logger.log(Level.WARNING, "No links available!");
                return;
            }
            /*while ((links = linksBuilder.call()) == null) 
            { // Loop until links become available
                Constants.logger.log(Level.WARNING, "No links available, retrying in: 5 " + "minutes");
                Thread.sleep(5 * 60000);
            }*/
            taskHandler.runTask(new UpdateTask(links)); // Run updaterTask
            Constants.logger.log(Level.INFO, "Checking for next updates in: {0} minutes", Constants.UPDATE_SCHEDULE_INTERVAL);
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
