/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.updater.checksums;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles checksums
 */
public class ChecksumsHandler 
{
    /**
     * Generate checksum example
     * 
     * @throws IOException 
     */
    public static void generateChecksumExample() throws IOException 
    {
        Checksum[] checksums = new Checksum[] 
        {
            //new Checksum("Example-1.1.jar", 1.1, "3307319ddc221f1b23e8a1445aef10d2d2308e0ec46977b3f17cbb15c0ef335b", true),
            //new Checksum("AnotherExample-1.2.jar", 1.2, "46073194dc221f1b23e8a1445aef10d3d2308e0ec46977b3f17cbb15c0ef335b", true)
            
             new Checksum.ChecksumBuilder()
                    .name("Example-1.0.jar")
                    .version(1.0)
                    .size(8139)
                    .execute(true)
                    .creationTime("2013-03-24 20:17:24")
                    .lastModifiedTime("2013-03-24 20:17:24")
                    .lastAccessTime("2020-12-27 14:53:30")
                    .MD5("2e55c05d3386889af97caae4517ac9df")
                    .SHA1("b90b6ac57cf27a2858eaa490d02ba7945d18ca7b")
                    .SHA256("3307319ddc221f1b23e8a1445aef10d2d2308e0ec46977b3f17cbb15c0ef335b")
                    .SHA384("f274993847fd46b8aa27e96c812fae0aacd0934fcd5670ba366b451b74079a21f295bb62ff676e7c7bdca13fe536d723")
                    .SHA512("fbefc32530253cc3fe5638da036a85567cdce9c9f748522725f58b349b07a37b474e16cec329b333b7f720e3c385a2ff7024d51d0a93bc26d201726f559e42f1")
                    .build(),
                
                    new Checksum.ChecksumBuilder()
                    .name("AnotherExample-1.1.jar")
                    .version(1.1)
                    .size(9539)
                    .execute(true)
                    .creationTime("2020-12-26 20:17:24")
                    .lastModifiedTime("2020-12-26 20:17:24")
                    .lastAccessTime("2020-12-27 14:53:30")
                    .MD5("3d55c35d3386889af97ceee4517ac9df")
                    .SHA1("z90b6ac55cf27a2858eaa490d02ba7945d18ca7b")
                    .SHA256("8307119ddc221f1b23e8a1446aef10d2d2308e0ec46977b3f17cbb15c0ef335b")
                    .SHA384("e274993847fd46b8aa27e96c812fae0aacd0934fcd5670ba366b451b74079a21f295bb62ff676e7c7bdca13fe536d723")
                    .SHA512("gbefc32530253cc3fe5638da036a85567cdce9c9f748522725f58b349b07a37b474e16cec329b333b7f720e3c385a2ff7024d51d0a93bc26d201726f559e42f1")
                    .build()
        };
        
        File checksumExample = new File("./Checksums_Example.json");
        try (Writer writer = new FileWriter(checksumExample)) 
        {
            Gson gson = new GsonBuilder()
                    .excludeFieldsWithoutExposeAnnotation()
                    .setPrettyPrinting()
                    .create();
            gson.toJson(checksums, writer);
            System.out.println("Saved checksums example in: " + checksumExample.getAbsolutePath());
        }
    }
    
    /**
     * Get all checksums as list from link/URL
     *
     * @param checksumsLink
     * @see Checksum.java
     * @see UpdaterTask.java
     * @return checksumList
     */
    public List getChecksumsList(String checksumsLink) 
    {
        List<Checksum> checksumsList = null;
        try 
        {
            URL url = new URL(checksumsLink);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.0.13) Gecko/2009073021 Firefox/3.0.13");
            connection.connect();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream())); // Read

            Gson gson = new GsonBuilder()
                    .excludeFieldsWithoutExposeAnnotation()
                    .create();
            
            Type listType = new TypeToken<List<Checksum>>() {}.getType(); // List type using variables from Checksum.class
            checksumsList = gson.fromJson(reader, listType); // Parse the checksums using listType
            connection.disconnect();
        } 
        catch (MalformedURLException ex) 
        {
            Logger.getLogger(ChecksumsHandler.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(ChecksumsHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return checksumsList;
    }
}
