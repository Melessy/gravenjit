package com.gravenjit.updater.unused;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;

public class Ping 
{
    public static Duration ping(String link) 
    {
    Instant startTime = Instant.now();
    try 
    {
        URL url = new URL(link); // Set the url using String link
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
	connection.setConnectTimeout(3000);
        connection.connect();
        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) 
        {
            return Duration.between(startTime, Instant.now());
        }
    } 
    catch (IOException e) 
    {
        // Host not available, nothing to do here
    }
    return null;
}
}
