/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.client.commands;

import com.gravenjit.client.ChannelHandler;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Handle inbound commands from server
 */
public class CommandHandler implements Runnable 
{
    private final ExecutorService commandExecutor = Executors.newSingleThreadExecutor(); // Executor to submit Future<Command>
    private final String[] args; // Arguments passed to the command
    private final String command; // Command, arg[0]

    /**
     * Initialize CommandHandler
     *
     * @param cmd
     */
    public CommandHandler(String cmd) 
    {
        this.args = cmd.split(" ");
        this.command = args[0];
    }

    @Override
    public void run() 
    {
        if (!Stream.of(Commands.values()).map(Enum::name).collect(Collectors.toSet()).contains(command)) { // Invalid command
            ChannelHandler.writeMessage("Invalid Command: " + command + "\n" + "Available Commands: -> " + Arrays.toString(Commands.values()));
            System.out.println("Invalid Command: " + command);
            return;
        }

        Command c = Commands.valueOf(command).getCommand(); // Get command

        if (Arrays.toString(args).contains("cancel")) 
        { // Cancel a command
            c.cancel(); // Run cancel on command
            ChannelHandler.writeMessage("Command: " + c + " is canceled");
            return;
        }

        Instant startTime = Instant.now(); // Start time
        ChannelHandler.writeMessage("Executing command: " + c); // Write message back to server

        Future<Command> future = (Future<Command>) commandExecutor.submit(() -> 
        { // Use Future to execute command and get its result
            c.execute(args); // Execute the command, @NOTE Might want to change the command interface, and execute commands as runnable
            return c; // Return the command
        });

        try 
        {
            future.get(5, TimeUnit.MINUTES); // Wait max x amount of time for command to finish
        } 
        catch (InterruptedException | ExecutionException | TimeoutException ex)
        {
            c.cancel(); // Cancel command
            ChannelHandler.writeMessage("Exception: " + ex + " " + command); // Write exception to server
            Logger.getLogger(CommandHandler.class.getName()).log(Level.SEVERE, null, ex);
            future.cancel(true); // Cancel future?
            commandExecutor.shutdownNow(); // Shutdown commandExecutor
        } 
        finally 
        {
            if (future.isDone()) 
            { // If command is completed/canceled
                Instant endTime = Instant.now(); // This is the endTime
                long totalTime = Duration.between(startTime, endTime).toMillis(); // This is the totalTime it took
                ChannelHandler.writeMessage("Command completed: " + c + " in " + totalTime + " ms"); // Write command completion to server
                commandExecutor.shutdownNow(); // Shutdown commandExecutor
            }
        }
    }
}
